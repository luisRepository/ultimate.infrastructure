﻿using System;
using System.ComponentModel;

namespace Ultimate.Core.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        #region Operations

        public static string GetDescription(this Enum value)
        {
            return value.GetDescription(string.Empty);
        }

        public static string GetDescription(this Enum value, string valueWhenEmpty)
        {
            if (value == null)
            {
                return valueWhenEmpty;
            }

            var attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : valueWhenEmpty;
        }

        #endregion
    }
}
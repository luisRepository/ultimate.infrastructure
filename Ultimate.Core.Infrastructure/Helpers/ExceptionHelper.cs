﻿using System;

namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class ExceptionHelper
    {
        #region Operations

        public static string GetMessagesAsString(Exception exception)
        {
            if(exception.InnerException == null)
            {
                return exception.Message;
            }

            return string.Concat(exception.Message, " | ", GetMessagesAsString(exception.InnerException));
        }

        #endregion
    }
}
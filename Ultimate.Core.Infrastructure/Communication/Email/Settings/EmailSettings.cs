﻿using Newtonsoft.Json;

namespace Ultimate.Core.Infrastructure.Communication.Email.Settings
{
    public class EmailSettings
    {
        #region Properties

        public string Provider { get; set; }

        public bool EnableService { get; set; }

        public string SendTo { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public bool SmtpEnableSsl { get; set; }

        public string SmtpUserName { get; set; }

        public string SmtpPassword { get; set; }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
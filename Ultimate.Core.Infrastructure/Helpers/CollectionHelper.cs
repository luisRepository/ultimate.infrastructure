﻿using System.Collections.Generic;
using System.Text;

namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class CollectionHelper
    {
        #region Operations

        public static string GetEnumerableAsString(IEnumerable<string> values)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string value in values)
            {
                if (sb.Length > 0)
                {
                    sb.Append(" | ");
                }

                sb.Append(value);
            }

            return sb.ToString();
        }

        #endregion
    }
}

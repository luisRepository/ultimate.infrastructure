﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Ultimate.Core.Infrastructure.Caching.Base;
using Ultimate.Core.Infrastructure.Helpers;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Messaging;
using Ultimate.Core.Infrastructure.Repository.Base;

namespace Ultimate.Core.Infrastructure.Repository.SqlServer
{
    public class SqlServerRepositoryBase<T> : RepositoryBase<T> where T : class
    {
        #region Identification

        private const string SOURCE = "SqlServerRepositoryBase";

        #endregion

        #region Attributes

        private readonly string connString;
        private readonly Dictionary<string, SqlParameter> parameters;

        #endregion

        #region Constructors

        public SqlServerRepositoryBase(string connString, ILoggingService loggingService, ICachingService cachingService = null) : base(loggingService, cachingService)
        {
            this.connString = connString;
            this.parameters = new Dictionary<string, SqlParameter>();
        }

        #endregion

        #region Properties

        protected Dictionary<string, SqlParameter> Parameters
        {
            get { return parameters; }
        }

        #endregion

        #region Operations

        public virtual Response Count(string transactionId, SqlCommand sqlCommand)
        {
            Log log = new Log(transactionId, SOURCE, $"Count({sqlCommand.CommandText})", IdentityHelper.GetIdentity());

            using (SqlConnection cnn = new SqlConnection(connString))
            {
                try
                {
                    sqlCommand.Connection = cnn;

                    cnn.Open();

                    long count = (long)sqlCommand.ExecuteScalar();

                    log.Level = Level.Information;
                    log.Detail = $"{count} records found.";

                    return new Response(ResponseCode.OK, true, "OK", count);
                }
                catch (Exception exception)
                {
                    log.Level = Level.Exception;
                    log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                    return new Response<string>(ResponseCode.InternalServerError, false, "Ocorreu uma exceção ao tentar processar a solicitação!");
                }
                finally
                {
                    base.LoggingService.Log(log);
                }
            }
        }

        public virtual Response<T> ExecuteReader(string transactionId, SqlCommand sqlCommand, ISqlServerDataReadable<T> sqlServerDataReadable)
        {
            Log log = new Log(transactionId, SOURCE, $"ExecuteReader({sqlCommand.CommandText})", IdentityHelper.GetIdentity());

            using (SqlConnection sqlConnection = new SqlConnection(connString))
            {
                try
                {
                    sqlCommand.Connection = sqlConnection;

                    sqlConnection.Open();

                    List<T> items = new List<T>();

                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            Response<T> readResponse = sqlServerDataReadable.Read(transactionId, sqlDataReader);

                            if (!readResponse.Succeeded)
                            {
                                throw new Exception(readResponse.Message);
                            }

                            items.Add(readResponse.Item);
                        }

                        if (items.Any())
                        {
                            log.Level = Level.Information;
                            log.Detail = $"{items.Count} records selected.";

                            return new Response<T>(ResponseCode.OK, true, "OK", items);
                        }
                        else
                        {
                            log.Level = Level.Information;
                            log.Detail = "Not found!";

                            return new Response<T>(ResponseCode.NotFound, false, "Not found!");
                        }
                    }
                }
                catch (Exception exception)
                {
                    log.Level = Level.Exception;
                    log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                    return new Response<T>(ResponseCode.InternalServerError, false, "Ocorreu uma exceção ao tentar processar a solicitação!");
                }
                finally
                {
                    base.LoggingService.Log(log);
                }
            }
        }

        public virtual Response ExecuteNonQuery(string transactionId, SqlCommand sqlCommand, bool ignoreNumberOfRowsAffected = false)
        {
            Log log = new Log(transactionId, SOURCE, $"ExecuteNonQuery({sqlCommand.CommandText})", IdentityHelper.GetIdentity());

            using (SqlConnection sqlConnection = new SqlConnection(connString))
            {
                try
                {
                    sqlCommand.Connection = sqlConnection;

                    sqlConnection.Open();

                    int numberOfRowsAffected = sqlCommand.ExecuteNonQuery();

                    if (ignoreNumberOfRowsAffected || numberOfRowsAffected > 0)
                    {
                        log.Level = Level.Information;
                        log.Detail = $"{numberOfRowsAffected} records affected.";

                        return new Response(ResponseCode.OK, true, "OK", numberOfRowsAffected);
                    }
                    else
                    {
                        log.Level = Level.Information;
                        log.Detail = "No records affected!";

                        return new Response<T>(ResponseCode.NotFound, false, "No records affected!");
                    }
                }
                catch (Exception exception)
                {
                    log.Level = Level.Exception;
                    log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                    return new Response<T>(ResponseCode.InternalServerError, false, "Ocorreu uma exceção ao tentar processar a solicitação!");
                }
                finally
                {
                    base.LoggingService.Log(log);
                }
            }
        }

        #endregion
    }
}
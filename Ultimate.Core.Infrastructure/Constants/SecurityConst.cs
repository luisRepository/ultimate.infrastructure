﻿namespace Ultimate.Core.Infrastructure.Constants
{
    public static class SecurityConst
    {
        #region Constants

        public const string AUTHORIZATIONHEADER = "Authorization";
        public const string AUTHENTICATIONKEY = "WWW-Authenticate";
        public const string AUTHENTICATIONSCHEME = "Basic";
        public const string TOKENIDHEADER = "TokenId";
        public const string TOKENHEADER = "Token";
        public const string IDENTITYNAME = "IdentityName";

        #endregion

    }
}

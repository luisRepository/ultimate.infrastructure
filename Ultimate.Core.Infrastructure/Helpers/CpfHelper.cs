﻿namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class CpfHelper
    {
        #region Operations

        public static bool IsValid(long cpf)
        {
            return IsValid(cpf.ToString().PadLeft(11, '0'));
        }

        public static bool IsValid(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            cpf = cpf.Trim().Replace(".", "").Replace("-", "");

            if (cpf.Length != 11)
            {
                return false;
            }

            for (int i = 0; i < 10; i++)
            {
                if (i.ToString().PadLeft(11, char.Parse(i.ToString())) == cpf)
                {
                    return false;
                }
            }

            tempCpf = cpf.Substring(0, 9);

            soma = 0;

            for (int i = 0; i < 9; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            }

            resto = soma % 11;

            resto = (resto < 2) ? 0 : 11 - resto;

            digito = resto.ToString();

            tempCpf = tempCpf + digito;

            soma = 0;

            for (int i = 0; i < 10; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            }

            resto = soma % 11;

            resto = (resto < 2) ? 0 : 11 - resto;

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        #endregion
    }
}
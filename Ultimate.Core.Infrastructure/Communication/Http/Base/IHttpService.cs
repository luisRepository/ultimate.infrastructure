﻿using System.Collections.Generic;
using Ultimate.Core.Infrastructure.Messaging;

namespace Ultimate.Core.Infrastructure.Communication.Http.Base
{
    public interface IHttpService
    {
        #region Operations

        Response<string> Get(Dictionary<string, string> headers, string uri);

        Response<string> Post(Dictionary<string, string> headers, string uri, string content, string contentType);

        Response<string> Put(Dictionary<string, string> headers, string uri, string content, string contentType);

        Response Delete(Dictionary<string, string> headers, string uri);

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Ultimate.Core.Infrastructure.Communication.Http.Base;
using Ultimate.Core.Infrastructure.Constants;
using Ultimate.Core.Infrastructure.Helpers;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Messaging;

namespace Ultimate.Core.Infrastructure.Communication.Http.Providers
{
    public class DefaultHttpService: IHttpService
    {
        #region Identification

        private const string SOURCE = "DefaultHttpService";

        #endregion

        #region Attributes

        private readonly ILoggingService loggingService;

        #endregion

        #region Constructors

        internal DefaultHttpService(ILoggingService loggingService)
        {
            this.loggingService = loggingService;
        }

        #endregion

        #region Operations

        public Response<string> Get(Dictionary<string, string> headers, string uri)
        {
            Log log = new Log(GetTransactionId(headers), SOURCE, $"Get({uri})", IdentityHelper.GetIdentity());

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    AdjustHttpRequestHeaders(httpClient, headers);

                    HttpResponseMessage httpResponseMessage = httpClient.GetAsync(uri).ConfigureAwait(false).GetAwaiter().GetResult();

                    Task<string> readTask = httpResponseMessage.Content.ReadAsStringAsync();

                    log.Detail = readTask.Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        log.Level = Level.Information;
                        return new Response<string>((ResponseCode)(int)httpResponseMessage.StatusCode, true, httpResponseMessage.ReasonPhrase, readTask.Result);
                    }
                    else
                    {
                        log.Level = Level.Error;
                        return new Response<string>((ResponseCode)(int)httpResponseMessage.StatusCode, false, httpResponseMessage.ReasonPhrase, readTask.Result);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                return new Response<string>(ResponseCode.InternalServerError, false, MessageConst.EXCEPTIONMESSAGE);
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        public Response<string> Post(Dictionary<string, string> headers, string uri, string content, string contentType)
        {
            Log log = new Log(GetTransactionId(headers), SOURCE, $"Post({uri}, {content}, {contentType})", IdentityHelper.GetIdentity());

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    AdjustHttpRequestHeaders(httpClient, headers);

                    HttpResponseMessage httpResponseMessage = httpClient.PostAsync(uri, new StringContent(content, Encoding.UTF8, contentType)).ConfigureAwait(false).GetAwaiter().GetResult();

                    Task<string> readTask = httpResponseMessage.Content.ReadAsStringAsync();

                    log.Detail = readTask.Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        log.Level = Level.Information;
                        return new Response<string>((ResponseCode)(int)httpResponseMessage.StatusCode, true, httpResponseMessage.ReasonPhrase, readTask.Result);
                    }
                    else
                    {
                        log.Level = Level.Error;
                        return new Response<string>((ResponseCode)(int)httpResponseMessage.StatusCode, false, httpResponseMessage.ReasonPhrase, readTask.Result);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                return new Response<string>(ResponseCode.InternalServerError, false, MessageConst.EXCEPTIONMESSAGE);
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        public Response<string> Put(Dictionary<string, string> headers, string uri, string content, string contentType)
        {
            Log log = new Log(GetTransactionId(headers), SOURCE, $"Put({uri}, {content}, {contentType})", IdentityHelper.GetIdentity());

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    AdjustHttpRequestHeaders(httpClient, headers);

                    HttpResponseMessage httpResponseMessage = httpClient.PutAsync(uri, new StringContent(content, Encoding.UTF8, contentType)).ConfigureAwait(false).GetAwaiter().GetResult();

                    Task<string> readTask = httpResponseMessage.Content.ReadAsStringAsync();

                    log.Detail = readTask.Result;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        log.Level = Level.Information;
                        return new Response<string>((ResponseCode)(int)httpResponseMessage.StatusCode, true, httpResponseMessage.ReasonPhrase, readTask.Result);
                    }
                    else
                    {
                        log.Level = Level.Error;
                        return new Response<string>((ResponseCode)(int)httpResponseMessage.StatusCode, false, httpResponseMessage.ReasonPhrase, readTask.Result);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                return new Response<string>(ResponseCode.InternalServerError, false, MessageConst.EXCEPTIONMESSAGE);
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        public Response Delete(Dictionary<string, string> headers, string uri)
        {
            Log log = new Log(GetTransactionId(headers), SOURCE, $"Delete({uri})", IdentityHelper.GetIdentity());

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    AdjustHttpRequestHeaders(httpClient, headers);

                    HttpResponseMessage httpResponseMessage = httpClient.DeleteAsync(uri).ConfigureAwait(false).GetAwaiter().GetResult();

                    log.Detail = httpResponseMessage.ReasonPhrase;

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        log.Level = Level.Information;
                        return new Response((ResponseCode)(int)httpResponseMessage.StatusCode, true, httpResponseMessage.ReasonPhrase);
                    }
                    else
                    {
                        log.Level = Level.Error;
                        return new Response((ResponseCode)(int)httpResponseMessage.StatusCode, false, httpResponseMessage.ReasonPhrase);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                return new Response<string>(ResponseCode.InternalServerError, false, MessageConst.EXCEPTIONMESSAGE);
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        #endregion

        #region Private operations

        private string GetTransactionId(Dictionary<string, string> headers)
        {
            return headers.ContainsKey(TransactionConst.TRANSACTIONIDHEADER) ? headers[TransactionConst.TRANSACTIONIDHEADER] : null;
        }

        private void AdjustHttpRequestHeaders(HttpClient httpClient, string transactionId, string scheme, string login, string password)
        {
            // Clear all headers
            httpClient.DefaultRequestHeaders.Clear();

            // Add current identity name
            httpClient.DefaultRequestHeaders.Add(SecurityConst.IDENTITYNAME, IdentityHelper.GetIdentity());

            // Add authentication header as necessary
            if (!string.IsNullOrWhiteSpace(scheme))
            {
                byte[] authBytes = Encoding.ASCII.GetBytes($"{login}:{password}");
                string authString = Convert.ToBase64String(authBytes);

                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(scheme, authString);
            }

            // Add transaction id header as necessary
            if (!string.IsNullOrEmpty(transactionId))
            {
                httpClient.DefaultRequestHeaders.Add(TransactionConst.TRANSACTIONIDHEADER, transactionId);
            }
        }

        private void AdjustHttpRequestHeaders(HttpClient httpClient, Dictionary<string, string> headers)
        {
            // Clear all headers
            httpClient.DefaultRequestHeaders.Clear();

            // Add key/value based headers
            foreach (KeyValuePair<string, string> header in headers)
            {
                httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
            }
        }

        #endregion
    }
}
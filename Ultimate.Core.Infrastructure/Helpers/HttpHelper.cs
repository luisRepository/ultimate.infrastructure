﻿using System;
using System.Text;

namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class HttpHelper
    {
        #region Operations

        public static string GetBasicAuthenticationValue(string login, string password)
        {
            byte[] authBytes = Encoding.ASCII.GetBytes($"{login}:{password}");

            return $"Basic {Convert.ToBase64String(authBytes)}";
        }

        #endregion

    }
}
﻿using Newtonsoft.Json;
using System;

namespace Ultimate.Core.Infrastructure.Caching.Settings
{
    public class CachingSettings
    {
        #region Attributes

        private string provider;
        private string host;
        private int port;
        private TimeSpan? defaultExpiryTime;

        #endregion

        #region Constructors

        public CachingSettings()
        {
            provider = null;
            host = null;
            port = 0;
            defaultExpiryTime = null;
        }

        #endregion

        #region Properties

        public string Provider
        {
            get => string.IsNullOrWhiteSpace(provider) ? null : provider.Trim().ToLower();
            set => provider = value;
        }

        public string Host
        {
            get => host;
            set => host = value;
        }

        public int Port
        {
            get => port;
            set => port = value;
        }

        public TimeSpan? DefaultExpiryTime
        {
            get => defaultExpiryTime;
            set => defaultExpiryTime = value;
        }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
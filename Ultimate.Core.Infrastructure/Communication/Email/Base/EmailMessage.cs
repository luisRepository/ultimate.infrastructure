﻿using Newtonsoft.Json;

namespace Ultimate.Core.Infrastructure.Communication.Email.Base
{
    public class EmailMessage
    {
        #region Properties

        public string From { get; set; }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
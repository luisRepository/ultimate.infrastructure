﻿using System;
using System.Net;
using System.Net.Mail;
using Ultimate.Core.Infrastructure.Communication.Email.Base;
using Ultimate.Core.Infrastructure.Communication.Email.Settings;

namespace Ultimate.Core.Infrastructure.Communication.Email.Providers
{
    public class SmtpEmailService: IEmailService
    {
        #region Attributes

        private readonly EmailSettings emailSettings;

        #endregion

        #region Constructors

        public SmtpEmailService(EmailSettings emailSettings)
        {
            this.emailSettings = emailSettings;
        }

        #endregion

        #region Operations

        public void Send(EmailMessage message)
        {
            if (emailSettings.EnableService)
            {
                SmtpClient smtpClient = new SmtpClient
                {
                    Host = emailSettings.SmtpHost,
                    Port = emailSettings.SmtpPort,
                    EnableSsl = emailSettings.SmtpEnableSsl,
                    Credentials = new NetworkCredential(emailSettings.SmtpUserName, emailSettings.SmtpPassword)
                };

                if (string.IsNullOrWhiteSpace(emailSettings.SendTo))
                {
                    MailMessage mailMessage = new MailMessage(message.From, message.To, message.Subject, message.Body)
                    {
                        IsBodyHtml = true
                    };

                    smtpClient.Send(mailMessage);
                }
                else
                {
                    string[] sendToList = emailSettings.SendTo.Split(';', StringSplitOptions.RemoveEmptyEntries);

                    foreach (var sendTo in sendToList)
                    {
                        MailMessage mailMessage = new MailMessage(message.From, sendTo, message.Subject, message.Body)
                        {
                            IsBodyHtml = true
                        };

                        smtpClient.Send(mailMessage);
                    }
                }
            }
        }

        #endregion
    }
}
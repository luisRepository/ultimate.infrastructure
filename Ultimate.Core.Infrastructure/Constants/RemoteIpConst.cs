﻿namespace Ultimate.Core.Infrastructure.Constants
{
    public static class RemoteIpConst
    {
        #region Constants

        public const string REMOTEIPHEADER = "X-Forwarded-For";

        #endregion
    }
}
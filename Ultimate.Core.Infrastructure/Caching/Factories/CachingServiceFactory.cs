﻿using System;
using Ultimate.Core.Infrastructure.Caching.Base;
using Ultimate.Core.Infrastructure.Caching.Providers;
using Ultimate.Core.Infrastructure.Caching.Settings;
using Ultimate.Core.Infrastructure.Logging.Base;

namespace Ultimate.Core.Infrastructure.Caching.Factories
{
    public class CachingServiceFactory
    {
        #region Attributes

        private static volatile CachingServiceFactory _instance;

        #endregion

        #region Constructors

        private CachingServiceFactory()
        {
        }

        #endregion

        #region Properties

        public static CachingServiceFactory Instance
        {
            get { return _instance ?? (_instance = new CachingServiceFactory()); }
        }

        #endregion

        #region Operations

        public ICachingService Build(CachingSettings cachingSettings, ILoggingService loggingService)
        {
            switch (cachingSettings.Provider)
            {
                case "redis":
                    return new RedisCachingService(cachingSettings, loggingService);

                default:
                    throw new ArgumentException("Invalid caching service!");
            }
        }

        #endregion
    }
}
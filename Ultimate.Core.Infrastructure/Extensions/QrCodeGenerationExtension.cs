﻿using SkiaSharp;
using SkiaSharp.QrCode.Image;
using System;
using System.IO;

namespace Ultimate.Core.Infrastructure.Extensions
{
    public class QrCodeGenerationExtension
    {
        public string GenerateBase64QrCode(string value)
        {
            var qrCode = new QrCode(value, new Vector2Slim(256, 256), SKEncodedImageFormat.Png);
            using var memoryStream = new MemoryStream();
            qrCode.GenerateImage(memoryStream);
            memoryStream.Position = 0;
            return Convert.ToBase64String(memoryStream.ToArray());
        }
    }
}
﻿namespace Ultimate.Core.Infrastructure.Constants
{
    public class MessageConst
    {
        #region Constants

        public const string ERRORMESSAGE = "Ocorreu um erro ao tentar processar a solicitação!";
        public const string EXCEPTIONMESSAGE = "Ocorreu uma exceção ao tentar processar a solicitação!";

        #endregion
    }
}
﻿using Microsoft.AspNetCore.Builder;

namespace Ultimate.Core.Infrastructure.Middleware
{
    public static class TokenValidationMiddlewareExtensions
    {
        #region Extensions

        public static IApplicationBuilder UseTokenValidation(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TokenValidationMiddleware>();
        }

        #endregion
    }
}
﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Text;
using Ultimate.Core.Infrastructure.Constants;
using Ultimate.Core.Infrastructure.Security;

namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class TokenHelper
    {
        #region Constants

        private const string GUIDPATTERN = "N";

        #endregion

        #region Operations

        public static string GenerateId()
        {
            return Guid.NewGuid().ToString(GUIDPATTERN);
        }

        public static Token GetTokenFromHttpRequest(HttpRequest request)
        {
            try
            {
                string base64Token = request.Headers[SecurityConst.TOKENHEADER];
                return JsonConvert.DeserializeObject<Token>(Encoding.UTF8.GetString(Convert.FromBase64String(base64Token)));
            }
            catch
            {
                return null;
            }
        }

        public static void SetTokenIntoHttpRequestHeader(HttpRequest request, Token token)
        {
            request.Headers[SecurityConst.TOKENHEADER] = Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(token)));
        }

        #endregion
    }
}
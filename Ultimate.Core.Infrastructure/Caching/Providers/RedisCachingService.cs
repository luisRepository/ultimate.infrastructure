﻿using StackExchange.Redis;
using System;
using Ultimate.Core.Infrastructure.Caching.Base;
using Ultimate.Core.Infrastructure.Caching.Settings;
using Ultimate.Core.Infrastructure.Constants;
using Ultimate.Core.Infrastructure.Helpers;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Messaging;

namespace Ultimate.Core.Infrastructure.Caching.Providers
{
    public class RedisCachingService : ICachingService
    {
        #region Identification

        private const string SOURCE = "RedisCachingService";

        #endregion

        #region Attributes

        private readonly CachingSettings cachingSettings;
        private readonly ILoggingService loggingService;

        #endregion

        #region Constructors

        internal RedisCachingService(CachingSettings cachingSettings, ILoggingService loggingService)
        {
            this.cachingSettings = cachingSettings;
            this.loggingService = loggingService;
        }

        #endregion

        #region Operations

        public Response<string> GetValue(string key, string transactionId = null)
        {
            Log log = new Log(transactionId, SOURCE, $"GetValue({key})", IdentityHelper.GetIdentity());

            try
            {
                ConfigurationOptions configurationOptions = new ConfigurationOptions();

                configurationOptions.EndPoints.Add(cachingSettings.Host, cachingSettings.Port);

                using (IConnectionMultiplexer redis = ConnectionMultiplexer.Connect(configurationOptions))
                {
                    IDatabase db = redis.GetDatabase();

                    string value = db.StringGet(key);

                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        log.Level = Level.Information;
                        log.Detail = value;

                        return new Response<string>(ResponseCode.OK, true, "OK", value);
                    }
                    else
                    {
                        log.Level = Level.Error;
                        log.Detail = "Chave não localizada!";

                        return new Response<string>(ResponseCode.NotFound, false, "Chave não localizada!");
                    }
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                return new Response<string>(ResponseCode.InternalServerError, false, MessageConst.EXCEPTIONMESSAGE);
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        public void SetValue(string key, string value, TimeSpan? expiry = null, string transactionId = null)
        {
            Log log = new Log(transactionId, SOURCE, $"SetValue({key}, {value}, {expiry})", IdentityHelper.GetIdentity());

            try
            {
                ConfigurationOptions configurationOptions = new ConfigurationOptions();

                configurationOptions.EndPoints.Add(cachingSettings.Host, cachingSettings.Port);

                using (IConnectionMultiplexer redis = ConnectionMultiplexer.Connect(configurationOptions))
                {
                    IDatabase db = redis.GetDatabase();

                    if (db.StringSet(key, value, expiry ?? cachingSettings.DefaultExpiryTime))
                    {
                        log.Level = Level.Information;
                        log.Detail = value;
                    }
                    else
                    {
                        log.Level = Level.Error;
                        log.Detail = MessageConst.ERRORMESSAGE;
                    }
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        public void DeleteKey(string key, string transactionId = null)
        {
            Log log = new Log(transactionId, SOURCE, $"DeleteKey({key})", IdentityHelper.GetIdentity());

            try
            {
                ConfigurationOptions configurationOptions = new ConfigurationOptions();

                configurationOptions.EndPoints.Add(cachingSettings.Host, cachingSettings.Port);

                using (IConnectionMultiplexer redis = ConnectionMultiplexer.Connect(configurationOptions))
                {
                    IDatabase db = redis.GetDatabase();

                    if (db.KeyDelete(key))
                    {
                        log.Level = Level.Information;
                        log.Detail = key;
                    }
                    else
                    {
                        log.Level = Level.Error;
                        log.Detail = MessageConst.ERRORMESSAGE;
                    }
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        #endregion
    }
}
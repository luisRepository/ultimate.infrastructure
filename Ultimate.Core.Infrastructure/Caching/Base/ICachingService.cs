﻿using System;
using Ultimate.Core.Infrastructure.Messaging;

namespace Ultimate.Core.Infrastructure.Caching.Base
{
    public interface ICachingService
    {
        #region Operations

        Response<string> GetValue(string key, string transactionId = null);

        void SetValue(string key, string value, TimeSpan? expiry = null, string transactionId = null);

        void DeleteKey(string key, string transactionId = null);

        #endregion
    }
}
﻿namespace Ultimate.Core.Infrastructure.Constants
{
    public static class TransactionConst
    {
        #region Constants

        public const string TRANSACTIONIDHEADER = "TransactionId";

        #endregion

    }
}
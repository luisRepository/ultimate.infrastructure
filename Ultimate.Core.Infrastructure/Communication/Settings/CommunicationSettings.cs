﻿using Newtonsoft.Json;

namespace Ultimate.Core.Infrastructure.Communication.Settings
{
    public class CommunicationSettings
    {
        #region Attributes

        private string httpServiceProvider;

        #endregion

        #region Constructors

        public CommunicationSettings()
        {
            httpServiceProvider = null;
        }

        #endregion

        #region Properties

        public string HttpServiceProvider
        {
            get => string.IsNullOrWhiteSpace(httpServiceProvider) ? null : httpServiceProvider.Trim().ToLower();
            set => httpServiceProvider = value;
        }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
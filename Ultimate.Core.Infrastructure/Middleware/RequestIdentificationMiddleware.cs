﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ultimate.Core.Infrastructure.Constants;
using Ultimate.Core.Infrastructure.Helpers;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Security;

namespace Ultimate.Core.Infrastructure.Middleware
{
    public class RequestIdentificationMiddleware
    {
        #region Identification

        private const string SOURCE = "RequestIdentificationMiddleware";

        #endregion

        #region Attributes

        private readonly ILoggingService loggingService;
        private readonly RequestDelegate nextRequestDelegate;

        #endregion

        #region Constructors

        public RequestIdentificationMiddleware(ILoggingService loggingService, RequestDelegate requestDelegate)
        {
            this.loggingService = loggingService;
            this.nextRequestDelegate = requestDelegate;
        }

        #endregion

        #region Operations

        public async Task Invoke(HttpContext httpContext)
        {
            // Get http request
            HttpRequest request = httpContext.Request;

            // Get http request headers
            string transactionId = request.Headers[TransactionConst.TRANSACTIONIDHEADER];

            Log log = new Log();

            try
            {
                // Set log attributes
                log.Source = SOURCE;
                log.Action = "Invoke()";
                log.TransactionId = transactionId;

                Token token = TokenHelper.GetTokenFromHttpRequest(request);

                if (token == null)
                {
                    log.Level = Level.Error;
                    log.Detail = "Token não informado!";
                    log.IdentityName = "Unauthenticated";

                    Unauthorize(httpContext.Response);

                    return;
                }

                IIdentity identity = new GenericIdentity(token.UserLogin);
                IPrincipal principal = new GenericPrincipal(identity, null);

                Thread.CurrentPrincipal = principal;

                log.Level = Level.Information;
                log.Detail = token.UserLogin;
                log.IdentityName = token.UserLogin;

                await nextRequestDelegate.Invoke(httpContext);
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);
                log.IdentityName = "Unauthenticated";
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        #endregion

        #region Private operations

        private void Unauthorize(HttpResponse response)
        {
            response.StatusCode = 401;
            response.Headers.Add(SecurityConst.AUTHENTICATIONKEY, SecurityConst.AUTHENTICATIONSCHEME);
        }

        #endregion
    }
}
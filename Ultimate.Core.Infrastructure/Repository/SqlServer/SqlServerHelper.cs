﻿using System;
using System.Data.SqlClient;

namespace Ultimate.Core.Infrastructure.Repository.SqlServer
{
    public static class SqlServerHelper
    {
        #region Operations

        public static DateTime? GetNullableDateTime(SqlDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            return reader.GetDateTime(ordinal);
        }

        public static string GetNullableString(SqlDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? null : reader.GetString(ordinal);
        }

        #endregion
    }
}
﻿using System;
using Ultimate.Core.Infrastructure.Communication.Http.Base;
using Ultimate.Core.Infrastructure.Communication.Http.Providers;
using Ultimate.Core.Infrastructure.Communication.Settings;
using Ultimate.Core.Infrastructure.Logging.Base;

namespace Ultimate.Core.Infrastructure.Communication.Http.Factories
{
    public class HttpServiceFactory
    {
        #region Attributes

        private static volatile HttpServiceFactory _instance;

        #endregion

        #region Constructors

        private HttpServiceFactory()
        {
        }

        #endregion

        #region Properties

        public static HttpServiceFactory Instance
        {
            get { return _instance ?? (_instance = new HttpServiceFactory()); }
        }

        #endregion

        #region Operations

        public IHttpService Build(CommunicationSettings communicationSettings, ILoggingService loggingService)
        {
            switch (communicationSettings.HttpServiceProvider)
            {
                case "default":
                    return new DefaultHttpService(loggingService);

                default:
                    throw new ArgumentException("Invalid http service!");
            }
        }

        #endregion
    }
}

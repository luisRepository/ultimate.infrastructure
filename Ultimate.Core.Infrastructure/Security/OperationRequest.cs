﻿namespace Ultimate.Core.Infrastructure.Security
{
    public enum OperationRequest
    {
        Read,
        Insert,
        Update,
        Delete,
        Execute
    }
}
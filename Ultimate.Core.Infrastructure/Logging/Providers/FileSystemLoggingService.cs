﻿using System;
using System.IO;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Logging.Settings;

namespace Ultimate.Core.Infrastructure.Logging.Providers
{
    public class FileSystemLoggingService: ILoggingService
    {
        #region Attributes

        private readonly LoggingSettings loggingSettings;

        private static readonly object _locker = new object();

        #endregion

        #region Constructors

        internal FileSystemLoggingService(LoggingSettings loggingSettings)
        {
            this.loggingSettings = loggingSettings;

            lock (_locker)
            {
                if (!Directory.Exists(loggingSettings.Path))
                {
                    Directory.CreateDirectory(loggingSettings.Path);
                }
            }
        }

        #endregion

        #region Operations

        public void Log(string transactionId, string source, string action, string identityName, Level level, string detail, string tag = null)
        {
            Log(new Log(source, action, identityName, level, detail, tag));
        }

        public void Log(Log log)
        {
            lock (_locker)
            {
                string file = GetFile();

                using (StreamWriter sw = File.AppendText(file))
                {
                    sw.WriteLine(log);
                }
            }
        }

        #endregion

        #region Private operations

        private string GetFile()
        {
            string fileName = string.Concat(DateTime.Today.ToString("yyyy-MM-dd"), ".txt");
            return Path.Combine(loggingSettings.Path, fileName);
        }

        #endregion
    }
}
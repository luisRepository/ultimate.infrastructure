﻿using Newtonsoft.Json;

namespace Ultimate.Core.Infrastructure.Logging.Settings
{
    public class LoggingSettings
    {
        #region Attributes

        private string provider;
        private string path;
        private string scheme;
        private string login;
        private string password;
        private string index;
        private string type;

        #endregion

        #region Constructors

        public LoggingSettings()
        {
            provider = null;
            path = null;
            scheme = null;
            login = null;
            password = null;
            index = null;
            type = null;
        }

        #endregion

        #region Properties

        public string Provider
        {
            get => string.IsNullOrWhiteSpace(provider) ? null : provider.Trim().ToLower();
            set => provider = value;
        }

        public string Path
        {
            get => path;
            set => path = value;
        }

        public string Scheme
        {
            get => scheme;
            set => scheme = value;
        }

        public string Login
        {
            get => login;
            set => login = value;
        }

        public string Password
        {
            get => password;
            set => password = value;
        }

        public string Index
        {
            get => index;
            set => index = value;
        }

        public string Type
        {
            get => type;
            set => type = value;
        }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
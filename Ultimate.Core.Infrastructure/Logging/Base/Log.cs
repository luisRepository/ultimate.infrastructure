﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using Ultimate.Core.Infrastructure.Helpers;

namespace Ultimate.Core.Infrastructure.Logging.Base
{
    public class Log
    {
        #region Constructors

        public Log()
        {
            this.Date = DateHelper.GetDefaultFormat(DateTime.Now);
        }

        public Log(string source, string action, string identityName) : this()
        {
            this.Source = source;
            this.Action = action;
            this.IdentityName = identityName;
        }

        public Log(string source, string action, string identityName, string tag) : this(source, action, identityName)
        {
            this.Tag = tag;
        }

        public Log(string source, string action, string identityName, Level level, string detail, string tag) : this(source, action, identityName, tag)
        {
            this.Level = level;
            this.Detail = detail;
        }

        #endregion

        #region Properties

        public string Date { get; set; }

        public string TransactionId { get; set; }

        public string Source { get; set; }

        public string Action { get; set; }

        public string IdentityName { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Level Level { get; set; }

        public string Detail { get; set; }

        public string Tag { get; set; }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Ultimate.Core.Infrastructure.Extensions
{
    public class Cripto
    {
        private string myKey;
        private TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
        private MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

        public Cripto()
        {
            myKey = "gluckkey";
        }

        public string DeCifra(string texto)
        {
            string DeCifraRet = default(string);
            des.Key = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(myKey));
            des.Mode = CipherMode.ECB;
            ICryptoTransform desdencrypt = des.CreateDecryptor();
            var buff = Convert.FromBase64String(texto);
            DeCifraRet = Encoding.UTF8.GetString(desdencrypt.TransformFinalBlock(buff, 0, buff.Length));
            return DeCifraRet;
        }

        public string Cifra(string texto)
        {
            string CifraRet = default(string);
            des.Key = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(myKey));
            des.Mode = CipherMode.ECB;
            ICryptoTransform desdencrypt = des.CreateEncryptor();
            var MyUTF8Encoding = new UTF8Encoding();
            var buff = Encoding.UTF8.GetBytes(texto);
            CifraRet = Convert.ToBase64String(desdencrypt.TransformFinalBlock(buff, 0, buff.Length));
            return CifraRet;
        }
    }
}
﻿namespace Ultimate.Core.Infrastructure.Logging.Base
{
    public interface ILoggingService
    {
        #region Operations

        void Log(string transactionId, string source, string action, string identityName, Level level, string detail, string tag = null);

        void Log(Log log);

        #endregion
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Text;
using Ultimate.Core.Infrastructure.Helpers;

namespace Ultimate.Core.Infrastructure.Security
{
    public class Token
    {
        #region Constructors

        public Token(string userLogin, string password, TimeSpan timeout, string userName)
        {
            Id = TokenHelper.GenerateId();
            UserLogin = userLogin;
            Password = password;
            SetTimeout(timeout);
            UserName = userName;
        }

        [JsonConstructor]
        public Token(string id, string userLogin, string password, string expirationDate, string userName)
        {
            Id = id;
            UserLogin = userLogin;
            Password = password;
            ExpirationDate = expirationDate;
            UserName = userName;
        }

        #endregion

        #region Properties

        public string Id { get; }

        public string UserLogin { get; private set; }

        public string UserName { get; set; }

        public string Password { get; private set; }

        public string ExpirationDate { get; private set; }

        #endregion

        #region Operations

        public void SetTimeout(TimeSpan timeout)
        {
            ExpirationDate = DateHelper.GetDefaultFormat(DateTime.Now.Add(timeout));
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public string ToBase64String()
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(this)));
        }

        #endregion
    }
}

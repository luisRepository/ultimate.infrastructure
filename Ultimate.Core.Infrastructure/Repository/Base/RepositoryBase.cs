﻿using Newtonsoft.Json;
using System;
using Ultimate.Core.Infrastructure.Caching.Base;
using Ultimate.Core.Infrastructure.Helpers;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Messaging;

namespace Ultimate.Core.Infrastructure.Repository.Base
{
    public abstract class RepositoryBase<T> where T : class
    {
        #region Identification

        private const string SOURCE = "RepositoryBase";

        #endregion

        #region Attributes

        private readonly ILoggingService loggingService;
        private readonly ICachingService cachingService;

        #endregion

        #region Constructors

        protected RepositoryBase(ILoggingService loggingService, ICachingService cachingService = null)
        {
            this.loggingService = loggingService;
            this.cachingService = cachingService;
        }

        #endregion

        #region Properties

        protected ILoggingService LoggingService
        {
            get => loggingService;
        }

        #endregion

        #region Operations

        protected Response<T> GetFromCache(string transactionId, string key)
        {
            Log log = new Log(transactionId, SOURCE, $"GetFromCache({key})", IdentityHelper.GetIdentity());

            try
            {
                if (cachingService == null)
                {
                    log.Level = Level.Error;
                    return new Response<T>(ResponseCode.NotImplemented, false, "Not implemented!");
                }

                Response<string> cacheResponse = cachingService.GetValue(key, transactionId);

                if (!cacheResponse.Succeeded)
                {
                    log.Level = Level.Error;
                    return new Response<T>(ResponseCode.NotFound, true, cacheResponse.Message);
                }

                log.Level = Level.Information;

                return new Response<T>(ResponseCode.OK, true, "OK", JsonConvert.DeserializeObject<T>(cacheResponse.Item));
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);

                return new Response<T>(ResponseCode.InternalServerError, false, "Ocorreu uma exceção ao tentar processar a solicitação!");
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        protected void SetOnCache(string transactionId, string key, T item)
        {
            Log log = new Log(transactionId, SOURCE, $"SetOnCache({key}, {item})", IdentityHelper.GetIdentity());

            cachingService?.SetValue(key, JsonConvert.SerializeObject(item), null, transactionId);

            loggingService.Log(log);
        }

        protected void DeleteFromCache(string transactionId, string key)
        {
            Log log = new Log(transactionId, SOURCE, $"DeleteFromCache({key})", IdentityHelper.GetIdentity());

            cachingService?.DeleteKey(key, transactionId);

            loggingService.Log(log);
        }

        #endregion
    }
}

﻿using System;

namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class DateHelper
    {
        #region Operations

        public static string GetDefaultFormat(DateTime date)
        {
            return date.ToString("o");
        }

        #endregion
    }
}
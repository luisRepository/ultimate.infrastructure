﻿namespace Ultimate.Core.Infrastructure.Communication.Email.Base
{
    public interface IEmailService
    {
        void Send(EmailMessage message);
    }
}
﻿using System;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Logging.Providers;
using Ultimate.Core.Infrastructure.Logging.Settings;

namespace Ultimate.Core.Infrastructure.Logging.Factories
{
    public class LoggingServiceFactory
    {
        #region Attributes

        private static volatile LoggingServiceFactory _instance;

        #endregion

        #region Constructors

        private LoggingServiceFactory()
        {
        }

        #endregion

        #region Properties

        public static LoggingServiceFactory Instance
        {
            get { return _instance ?? (_instance = new LoggingServiceFactory()); }
        }

        #endregion

        #region Operations

        public ILoggingService Build(LoggingSettings loggingSettings)
        {
            switch (loggingSettings.Provider)
            {
                case "filesystem":
                    return new FileSystemLoggingService(loggingSettings);

                default:
                    throw new ArgumentException("Invalid logging service!");
            }
        }

        #endregion
    }
}
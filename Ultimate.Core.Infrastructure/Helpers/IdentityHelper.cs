﻿using System.Threading;

namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class IdentityHelper
    {
        #region Operations

        public static string GetIdentity()
        {
            if (Thread.CurrentPrincipal == null)
            {
                return "Unauthenticated";
            }

            return Thread.CurrentPrincipal.Identity == null ? "Unauthenticated" : Thread.CurrentPrincipal.Identity.Name;
        }

        #endregion
    }
}
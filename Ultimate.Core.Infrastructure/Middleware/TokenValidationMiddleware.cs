﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Ultimate.Core.Infrastructure.Communication.Http.Base;
using Ultimate.Core.Infrastructure.Constants;
using Ultimate.Core.Infrastructure.Helpers;
using Ultimate.Core.Infrastructure.Logging.Base;
using Ultimate.Core.Infrastructure.Messaging;
using Ultimate.Core.Infrastructure.Security;

namespace Ultimate.Core.Infrastructure.Middleware
{
    public class TokenValidationMiddleware
    {
        #region Identification

        private const string SOURCE = "TokenValidationMiddleware";

        #endregion

        #region Attributes

        private readonly ILoggingService loggingService;
        private readonly IHttpService httpService;
        private readonly RequestDelegate nextRequestDelegate;

        #endregion

        #region Constructors

        public TokenValidationMiddleware(ILoggingService loggingService, IHttpService httpService, RequestDelegate requestDelegate)
        {
            this.loggingService = loggingService;
            this.httpService = httpService;
            this.nextRequestDelegate = requestDelegate;
        }

        #endregion

        #region Operations

        public async Task Invoke(HttpContext httpContext)
        {
            // Get http request
            HttpRequest request = httpContext.Request;

            // Get http response
            HttpResponse response = httpContext.Response;

            // Get response body
            Stream originalBody = response.Body;

            // Get http request headers
            string transactionId = request.Headers[TransactionConst.TRANSACTIONIDHEADER];
            string tokenId = request.Headers[SecurityConst.TOKENIDHEADER];

            Log log = new Log();

            try
            {
                // Set log attributes
                log.Source = SOURCE;
                log.Action = "Invoke()";
                log.TransactionId = transactionId;

                if (string.IsNullOrWhiteSpace(tokenId))
                {
                    log.Level = Level.Error;
                    log.Detail = "Token não informado!";
                    log.IdentityName = "Unauthenticated";

                    Unauthorize(httpContext.Response);

                    return;
                }

                Dictionary<string, string> headers = new Dictionary<string, string>();

                headers[TransactionConst.TRANSACTIONIDHEADER] = transactionId;
                headers[SecurityConst.TOKENIDHEADER] = tokenId;

                Response<string> httpServiceResponse = httpService.Get(headers, $"{Environment.GetEnvironmentVariable("SEGUROSSYSTEM_PORTAL_GATEWAYURL")}/api/autenticacao");

                if (!httpServiceResponse.Succeeded)
                {
                    log.Level = Level.Error;
                    log.Detail = httpServiceResponse.Message;
                    log.IdentityName = "Unauthenticated";

                    Unauthorize(httpContext.Response);

                    return;
                }

                Token token = JsonConvert.DeserializeObject<Token>(httpServiceResponse.Item);

                IIdentity identity = new GenericIdentity(token.UserLogin);
                IPrincipal principal = new GenericPrincipal(identity, null);

                Thread.CurrentPrincipal = principal;

                log.Level = Level.Information;
                log.Detail = token.UserLogin;
                log.IdentityName = token.UserLogin;

                TokenHelper.SetTokenIntoHttpRequestHeader(request, token);

                string responseBody = null;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    response.Body = memoryStream;

                    await nextRequestDelegate(httpContext);

                    memoryStream.Position = 0L;

                    responseBody = new StreamReader(memoryStream).ReadToEnd();
                    responseBody = responseBody.Replace("[tokenId]", token.Id);

                    string contentType = response.ContentType;

                    response.Clear();

                    response.ContentType = contentType;
                    response.Body = originalBody;

                    await response.WriteAsync(responseBody);
                }
            }
            catch (Exception exception)
            {
                log.Level = Level.Exception;
                log.Detail = ExceptionHelper.GetMessagesAsString(exception);
                log.IdentityName = "Unauthenticated";
            }
            finally
            {
                loggingService.Log(log);
            }
        }

        #endregion

        #region Private operations

        private void Unauthorize(HttpResponse response)
        {
            response.StatusCode = 401;
            response.Headers.Add(SecurityConst.AUTHENTICATIONKEY, SecurityConst.AUTHENTICATIONSCHEME);
        }

        #endregion
    }
}
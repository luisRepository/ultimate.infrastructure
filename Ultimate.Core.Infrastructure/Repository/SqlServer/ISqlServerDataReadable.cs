﻿using System.Data.SqlClient;
using Ultimate.Core.Infrastructure.Messaging;

namespace Ultimate.Core.Infrastructure.Repository.SqlServer
{
    public interface ISqlServerDataReadable<T> where T : class
    {
        #region Operations

        Response<T> Read(string transactionId, SqlDataReader sqlDataReader);

        #endregion
    }
}
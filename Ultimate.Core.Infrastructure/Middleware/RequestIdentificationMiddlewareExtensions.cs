﻿using Microsoft.AspNetCore.Builder;

namespace Ultimate.Core.Infrastructure.Middleware
{
    public static class RequestIdentificationMiddlewareExtensions
    {
        #region Extensions

        public static IApplicationBuilder UseRequestIdentification(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestIdentificationMiddleware>();
        }

        #endregion
    }
}
﻿using System;
using Ultimate.Core.Infrastructure.Communication.Email.Base;
using Ultimate.Core.Infrastructure.Communication.Email.Providers;
using Ultimate.Core.Infrastructure.Communication.Email.Settings;

namespace Ultimate.Core.Infrastructure.Communication.Email.Factories
{
    public class EmailServiceFactory
    {
        #region Attributes

        private static volatile EmailServiceFactory instance;

        #endregion

        #region Contructors

        public EmailServiceFactory()
        {
        }

        #endregion

        #region Properties

        public static EmailServiceFactory Instance
        {
            get { return instance ?? (instance = new EmailServiceFactory()); }
        }

        #endregion

        #region Operations

        public IEmailService Build(EmailSettings emailSettings)
        {
            switch (emailSettings.Provider)
            {
                case "Smtp":
                    return new SmtpEmailService(emailSettings);

                default:
                    throw new ArgumentException("Invalid Email service!");
            }
        }

        #endregion
    }
}
﻿using System;
using System.Globalization;

namespace Ultimate.Core.Infrastructure.Extensions
{
    public static class ConvertExtensions
    {
        #region Operations

        public static T To<T>(this object value)
        {
            return (T)To(value, typeof(T));
        }

        public static object To(this object value, Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            // return null if the value is null or DBNull
            if (value == null || value is DBNull || value.ToString().Trim() == string.Empty)
            {
                return null;
            }

            // non-nullable types, which are not supported by Convert.ChangeType(),
            // unwrap the types to determine the underlying time
            if (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                type = Nullable.GetUnderlyingType(type);
            }

            // deal with conversion to enum types when input is a string
            if (type.IsEnum && value is string)
            {
                return Enum.Parse(type, value as string);
            }

            // deal with conversion to enum types when input is a integral primitive
            if (value != null && type.IsEnum && value.GetType().IsPrimitive &&
                !(value is bool) && !(value is char) &&
                !(value is float) && !(value is double))
            {
                return Enum.ToObject(type, value);
            }

            // use Convert.ChangeType() to do all other conversions
            return Convert.ChangeType(value, type, CultureInfo.InvariantCulture);
        }

        public static string ToValueWhenEmpty(this string value, string valueWhenEmpty)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return valueWhenEmpty;
            }

            return value;
        }

        public static object ToValueWhenEmpty(this object value, object valueWhenEmpty)
        {
            if (value == null || value is DBNull || value.ToString().Trim() == string.Empty)
            {
                return valueWhenEmpty;
            }

            return value;
        }

        #endregion
    }
}
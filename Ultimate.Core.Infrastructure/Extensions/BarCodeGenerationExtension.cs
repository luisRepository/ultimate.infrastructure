﻿using SkiaSharp;
using System;
using System.IO;
using ZXing;

namespace Ultimate.Core.Infrastructure.Extensions
{
    public class BarCodeGenerationExtension
    {
        public string GenerateBase64BarCode(string value)
        {
            const int WIDTH = 256;
            const int HEIGHT = 100;
            const int BARCODE_HEIGHT = HEIGHT - 40;
            var barCodeWriter = new ZXing.SkiaSharp.BarcodeWriter
            {
                Format = BarcodeFormat.CODE_128,
                Options =
        {
            Height = BARCODE_HEIGHT,
            Width = WIDTH,
            PureBarcode = true
        }
            };

            using var background = new SKBitmap(WIDTH, HEIGHT);
            using var canvas = new SKCanvas(background);
            canvas.Clear(new SKColor(255, 255, 255));

            using var barcodeBitmap = barCodeWriter.Write(value);
            canvas.DrawBitmap(barcodeBitmap, 0, 0);

            using var paint = new SKPaint
            {
                TextSize = 16,
                TextAlign = SKTextAlign.Center,
                Color = new SKColor(0, 0, 0),
                Typeface = SKTypeface.FromFile("Resources/CascadiaCode.ttf")
            };
            canvas.DrawText(value, WIDTH / 2, HEIGHT - 20, paint);
            var ms = new MemoryStream();
            background.Encode(SKEncodedImageFormat.Jpeg, 100).SaveTo(ms);
            _ = ms.Seek(0, SeekOrigin.Begin);
            return Convert.ToBase64String(ms.ToArray());
        }
    }
}
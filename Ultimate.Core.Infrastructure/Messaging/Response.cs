﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Ultimate.Core.Infrastructure.Messaging
{
    public class Response
    {
        #region Attributes

        private readonly ResponseCode responseCode;
        private readonly bool succeeded;
        private readonly string message;

        private long count;

        #endregion

        #region Constructors

        public Response(ResponseCode responseCode, bool succeeded, string message)
        {
            this.responseCode = responseCode;
            this.succeeded = succeeded;
            this.message = message;
            this.count = 0L;
        }

        public Response(ResponseCode responseCode, bool succeeded, string message, long count) : this(responseCode, succeeded, message)
        {
            this.count = count;
        }

        #endregion

        #region Properties

        public ResponseCode ResponseCode
        {
            get => responseCode;
        }

        public bool Succeeded
        {
            get => succeeded;
        }

        public string Message
        {
            get => message;
        }

        public long Count
        {
            get => count;
            set => count = value;
        }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion

    }

    public class Response<T> : Response where T : class
    {
        #region Attributes

        private readonly List<T> items = new List<T>();

        #endregion

        #region Constructors

        public Response(ResponseCode responseCode, bool succeeded, string message) : base(responseCode, succeeded, message)
        {
        }

        public Response(ResponseCode responseCode, bool succeeded, string message, T item) : base(responseCode, succeeded, message)
        {
            this.items.Add(item);
            base.Count = items.Count;
        }

        public Response(ResponseCode responseCode, bool succeeded, string message, List<T> items) : base(responseCode, succeeded, message)
        {
            this.items = items;
            base.Count = items.Count;
        }

        #endregion

        #region Properties

        public T Item
        {
            get => items.FirstOrDefault();
        }

        public List<T> Items
        {
            get => items;
        }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
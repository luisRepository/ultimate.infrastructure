﻿using Newtonsoft.Json;

namespace Ultimate.Core.Infrastructure.Security
{
    public class AuthorizationRequest
    {
        #region Attributes

        private string nomeRecurso;
        private OperationRequest operationRequest;

        #endregion

        #region Constructors

        public AuthorizationRequest()
        {
            nomeRecurso = null;
            operationRequest = OperationRequest.Read;
        }

        #endregion

        #region Properties

        public string NomeRecurso
        {
            get => nomeRecurso;
            set => nomeRecurso = value;
        }

        public OperationRequest OperationRequest
        {
            get => operationRequest;
            set => operationRequest = value;
        }

        #endregion

        #region Operations

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}

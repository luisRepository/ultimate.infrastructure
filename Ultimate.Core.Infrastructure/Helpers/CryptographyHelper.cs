﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Ultimate.Core.Infrastructure.Helpers
{
    public static class CryptographyHelper
    {
        #region Operations

        public static string GetMd5Hash(string value)
        {
            string md5Hash = string.Empty;

            if (string.IsNullOrWhiteSpace(value))
            {
                return md5Hash;
            }

            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(value));
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("x2"));
                }

                md5Hash = sb.ToString();
            }

            return md5Hash;
        }

        public static string GetSha256Hash(string value)
        {
            string sha256Hash = string.Empty;

            if (string.IsNullOrWhiteSpace(value))
            {
                return sha256Hash;
            }

            using (SHA256 sha = SHA256.Create())
            {
                byte[] hash = sha.ComputeHash(Encoding.UTF8.GetBytes(value));
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("x2"));
                }

                sha256Hash = sb.ToString();
            }

            return sha256Hash;
        }

        public static bool CompareSha256Hash(string value, string hash)
        {
            try
            {
                string computedHash = GetSha256Hash(value);

                StringComparer sc = StringComparer.Ordinal;

                return (sc.Compare(computedHash, hash) == 0);
            }
            catch
            {
                return false;
            }
        }

        public static string DecryptTripleDES(string value, string key)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            var tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider
            {
                Key = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(key)),
                Mode = CipherMode.ECB
            };

            var inputBuffer = Convert.FromBase64String(value);

            return Encoding.UTF8.GetString(tripleDESCryptoServiceProvider.CreateDecryptor().TransformFinalBlock(inputBuffer, 0, inputBuffer.Length));
        }

        #endregion
    }
}

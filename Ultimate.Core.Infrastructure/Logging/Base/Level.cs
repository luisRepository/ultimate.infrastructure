﻿namespace Ultimate.Core.Infrastructure.Logging.Base
{
    public enum Level
    {
        System = -2,
        Debug = -1,
        Information = 0,
        Warning = 1,
        Error = 2,
        Exception = 3
    }
}